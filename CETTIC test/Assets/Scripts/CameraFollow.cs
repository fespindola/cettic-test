﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    [SerializeField] private float offsetZ = 0f;            // offset on z axis
    [SerializeField] private float offsetY = 0f;            // offset on y axis

    // --------------------------------<>
    void FixedUpdate()
    {
        float posX = player.transform.position.x;
        float posY = player.transform.position.y + offsetY;
        float posZ = player.transform.position.z + offsetZ;

        transform.position = new Vector3(posX, posY, posZ);
    }
}
