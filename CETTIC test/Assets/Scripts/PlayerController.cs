﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed;               // player movement speed
    [SerializeField] private float spinSpeed;           // player rotation speed

    private float movInput;
    private float turnInput;

    GameManager gameManger;

    Animator animator;
    Rigidbody rigidBody;

    // --------------------------------<>
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
    }

    // --------------------------------< Update is called once per frame >
    void Update()
    {
        movInput = Input.GetAxis("Vertical");
        turnInput = Input.GetAxis("Horizontal");

        if (movInput > 0.1f && GameManager.Instance.OnStart)
        {
            animator.SetBool("IsWalking", true);
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }
    }

    // --------------------------------< if onStart is true then walk and spin >
    void FixedUpdate()
    {
        if (GameManager.Instance.OnStart)
        {
            IsWalking();
            IsSpinning();
        }
    }

    // --------------------------------< when the player is walking >
    private void IsWalking()
    {
        Vector3 movement = transform.forward * movInput * speed * Time.deltaTime;
        rigidBody.MovePosition(rigidBody.position + movement);
    }

    // --------------------------------< when the player is spinning >
    private void IsSpinning()
    {
        float turn = turnInput * spinSpeed * Time.deltaTime;

        Quaternion spinRotation = Quaternion.Euler(0f, turn, 0f);
        rigidBody.MoveRotation(rigidBody.rotation * spinRotation);
    }

}
