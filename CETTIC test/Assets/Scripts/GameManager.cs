﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;

    [SerializeField] private Text timer;                    // timer text GUI
    [SerializeField] private Text coinsCounter;             // counter text GUI
    [SerializeField] private float time;                    // time in seconds
    [SerializeField] private float coinScore;               // coins acumulated

    public GameObject pointsGui;                            // points screen
    public GameObject gameOverGui;                          // gameover screen
    public GameObject wonGui;                               // won screen

    public AudioClip audioClip;                             // won sound
    AudioSource audioSource;

    private bool onStart = true;

    // --------------------------------<>
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(GameManager)) as GameManager;
            }
            return instance;
        }
    }

    // --------------------------------<>
    public bool OnStart
    {
        get { return onStart; }
    }

    // --------------------------------<>
    void Start()
    {
        pointsGui.SetActive(true);
        coinsCounter.text = coinScore.ToString();
        audioSource = GetComponent<AudioSource>();
    }

    // --------------------------------< Update is called once per frame >
    void Update()
    {
        if (onStart == true)
        {
            OnStartgame();
        }
    }

    // --------------------------------< add a coin when you touch it >
    public void AddCoin()
    {
        coinScore = coinScore + 1;
        coinsCounter.text = coinScore.ToString();

        if (coinScore == 11)
        {
            wonGui.SetActive(true);
        }
    }

    // --------------------------------< if the time is zero then game over >
    void GameOverScreen()
    {
        gameOverGui.SetActive(true);
        pointsGui.SetActive(false);
    }

    // --------------------------------< start the timer >
    void OnStartgame()
    {
        time = time - Time.deltaTime;
        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        timer.text = "Time: 0" + minutes.ToString() + ":" + seconds.ToString().PadLeft(2, '0');

        if (time <= 0)
        {
            onStart = false;
            GameOverScreen();
        }
        else if (time >= 0 && coinScore == 11)
        {
            onStart = false;
            wonGui.SetActive(true);
            audioSource.PlayOneShot(audioClip);
        }
    }

    // --------------------------------< start the game again >
    public void OnContinue()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
