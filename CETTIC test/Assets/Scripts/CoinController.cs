﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{
    [SerializeField] private float rotationSpeed = 150f;            // constant rotation
    [SerializeField] private float rotationAnimSpeed = 6.0f;        // rotation when anim was triggered
    [SerializeField] private float timeToDestroy = 1f;              // time to destroy the object after animation
    [SerializeField] GameObject parent;                             // the parent of this object

    public GameManager gameManager;
    
    AudioSource audioSource;                                        // sound of the coin
    Animator animator;

    // --------------------------------< use this for initialization >
    void Start()
    {
        animator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    // --------------------------------< Update is called once per frame >
    void Update()
    {
        this.gameObject.transform.Rotate(new Vector3(0f, 0f, rotationSpeed * Time.deltaTime));
    }

    // --------------------------------<>
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            animator.SetTrigger("CoinGot");
            StartCoroutine(GetCoin());
            gameManager.AddCoin();
            audioSource.Play();
        }
    }

    // --------------------------------< if the character get a coin then destroy the object >
    IEnumerator GetCoin()
    {
        rotationSpeed = rotationSpeed * rotationAnimSpeed;

        yield return new WaitForSeconds(timeToDestroy);
        Destroy(this.gameObject);
        Destroy(parent.gameObject);
    }
}
